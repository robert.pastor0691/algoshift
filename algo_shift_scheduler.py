
# Copyright 2010-2018 Google LLC
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless weekly_cover_demands  by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""nurse scheduling problem with shift requests."""


from __future__ import print_function

import argparse
import sys
import os
import json
import datetime
import re
from dateutil.parser import parse
# from datetime import datetime
from collections import defaultdict
import asyncio
from os.path import join, dirname
from google.protobuf import text_format
from pymongo import MongoClient
# pprint library is used to make the output look more pretty
from pprint import pprint
from dotenv import load_dotenv, dotenv_values, find_dotenv
from pathlib import Path
import numpy as np
import itertools as it
# import datetime
from datetime import date
import pandas as pd
from ortools.sat.python import cp_model


def main():
    dates = {'start': '2021-02-01T00:00:00.000Z',
             'end': '2021-02-27T00:00:00.000Z'}
    startschedule = parse(dates['start'])
    endschedule = parse(dates['end'])
    num_days = endschedule - startschedule
    all_days = range(num_days.days)
    agents = [
        "Tom", "David", "Jeremy", "Ron", "Joe", "Bill", "Fred", "Bob", "Mario",
        "Ed", "Carol", "Janet", "Tracy", "Marilyn", "Carolyn", "Cathy", "Inez",
        "Jean", "Heather", "Juliet",
        "Lonny", "Brianna", "Eldora", "Sandra", "Annita", "Josefa",
        "Josie", "Lon", "Neville", "Nga", "Candi", "Romeo", "Thanh",
        "Emanuel", "Shari", "Carina", "Ralph", "Charlotte", "Nelson", "Karena"
    ]
    num_agents = len(agents)  # number of nurses
    all_agents = range(num_agents)
    shifts = ['AM', 'PM', 'EV']
    num_shifts = len(shifts)
    all_shifts = range(num_shifts)
    weekly_cover_demands = np.array([
        [[2, 1, 1], [2, 1, 1], [2, 1, 1], [2, 1, 1],
            [2, 1, 1], [2, 1, 1], [2, 1, 1]],
        [[1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1],
            [1, 1, 1], [1, 1, 1], [1, 1, 1]],
        [[1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1],
            [1, 1, 1], [1, 1, 1], [1, 1, 1]],
        [[2, 2, 1], [2, 2, 1], [2, 2, 1], [2, 2, 1],
         [2, 2, 1], [2, 2, 1], [2, 2, 1]],
        [[2, 1, 0], [2, 1, 0], [2, 1, 0], [2, 1, 0],
         [2, 1, 0], [2, 1, 0], [2, 1, 0]],
        [[1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1],
         [1, 1, 1], [1, 1, 1], [1, 1, 1]],
        [[1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1],
         [1, 1, 1], [1, 1, 1], [1, 1, 1]],
        [[1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1],
         [1, 1, 1], [1, 1, 1], [1, 1, 1]],
        [[2, 2, 1], [2, 2, 1], [2, 2, 1], [2, 2, 1],
         [2, 2, 1], [2, 2, 1], [2, 2, 1]],
        [[2, 1, 0], [2, 1, 0], [2, 1, 0], [2, 1, 0], [2, 1, 0], [2, 1, 0], [2, 1, 0]]
    ])
    num_sites = np.shape(weekly_cover_demands)[0]
    all_sites = range(num_sites)

    model = cp_model.CpModel()
    # Creates shift variables.
    # shift[(n, d, s, l)]: agent 'n' works shift 's' on day 'd' at location 'l'.
    shift = {}
    for n in all_agents:
        for s in all_shifts:  # range(num_shifts):
            for d in all_days:
                for l in all_sites:
                    shift[(n, s, d, l)] = model.NewBoolVar(
                        'shift_n%is%id%il%i' % (n, s, d, l))

    for n in all_agents:
        for l in all_sites:
            for d in all_days:
                model.Add(sum(shift[(n, s, d, l)]
                              for s in range(num_shifts)) <= 1)
    # for n in all_agents:
    #     for d in all_days:
    #         model.Add(sum(shift[(n, s, d, l)]
    #                       for s in range(num_shifts) for l in all_sites) <= 1)
    # the  numbers of agents on day d in shift s at site l should be less or equal to z
    for l in all_sites:
        for d in range(7):
            for s in range(num_shifts):
                model.Add(sum(shift[(n, s, d, l)]
                              for n in all_agents) == weekly_cover_demands[l, d, s])

    for n in all_agents:
        week = []
        for d in all_days:
            for l in all_sites:
                for s in all_shifts:
                    week.append(shift[(n, s, d, l)])
            # week.append(sum(shifts[(n,d,s)] for s in all_shifts))
    model.Add(sum(week) <= 5)
    # min_shifts_per_agent = (num_shifts * num_days.days) // num_agents
    # print(min_shifts_per_agent)
    # max_shifts_per_agent = min_shifts_per_agent + 1
    # for n in all_agents:
    #     num_shifts_worked = sum(
    #         shift[(n, s, d, l)] for l in all_sites for d in all_days for s in all_shifts)
    #     model.Add(min_shifts_per_agent <= num_shifts_worked)
    #     model.Add(num_shifts_worked <= max_shifts_per_agent)
    # [START objective]
    # pylint: disable=g-complex-comprehension
    model.Maximize(
        sum(shift[(n, s, d, l)]
            for l in all_sites for d in all_days for n in all_agents for s in range(num_shifts)))

    # Creates the solver and solve.
    solver = cp_model.CpSolver()
    solver.Solve(model)
    # Print solution.
    # if status == cp_model.OPTIMAL or status == cp_model.FEASIBLE:
    for l in all_sites:
        for d in all_days:
            for s in all_shifts:
                for n in all_agents:
                    if solver.Value(shift[(n, s, d, l)]) == 1:
                        with open('___output.txt', 'a') as f:
                            print("nurse {} covers shift {} on day {}  at site {}".format(
                                agents[n], s, startschedule + datetime.timedelta(days=d), l), file=f)
                        # print("nurse {} covers shift {} on day {}  at site {}".format(
                        #     agents[n], s, startschedule + datetime.timedelta(days=d), l))

    # Statistics.
    print()
    print('Statistics')
    print('  - conflicts       : %i' %
          solver.NumConflicts())
    print('  - branches        : %i' %
          solver.NumBranches())
    print('  - wall time       : %f s' % solver.WallTime())
    # print('  - solutions found : %i' % solution_printer.solution_count())


if __name__ == '__main__':
    main()
